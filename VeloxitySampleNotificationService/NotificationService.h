//
//  NotificationService.h
//  VeloxitySampleNotificationService
//
//  Copyright © 2019 Veloxity. All rights reserved.
//

#import <UserNotifications/UserNotifications.h>
#import <VeloxitySDK/Veloxity.h>

@interface NotificationService : UNNotificationServiceExtension

@end

