//
//  NotificationService.m
//  VeloxitySampleNotificationService
//
//  Copyright © 2019 Veloxity. All rights reserved.
//

#import "NotificationService.h"

@interface NotificationService ()

@property (nonatomic, strong) void (^contentHandler)(UNNotificationContent* contentToDeliver);
@property (nonatomic, strong) UNNotificationRequest* receivedRequest;
@property (nonatomic, strong) UNMutableNotificationContent* bestAttemptContent;

@end

@implementation NotificationService

- (void)didReceiveNotificationRequest:(UNNotificationRequest*)request withContentHandler:(void (^)(UNNotificationContent* _Nonnull))contentHandler
{
    NSLog(@"called Notification Service didReceiveNotificationRequest");
    self.receivedRequest = request;
    self.contentHandler = contentHandler;
    self.bestAttemptContent = [request.content mutableCopy];
    
    [[Veloxity sharedInstance] didReceiveNotificationExtensionRequest:self.receivedRequest withMutableNotificationContent:self.bestAttemptContent];
}

- (void)serviceExtensionTimeWillExpire
{
    // Called just before the extension will be terminated by the system.
    // Use this as an opportunity to deliver your "best attempt" at modified content, otherwise the original push payload will be used.
    NSLog(@"called Notification Service serviceExtensionTimeWillExpire");
    
    [[Veloxity sharedInstance] serviceExtensionTimeWillExpireRequest:self.receivedRequest withMutableNotificationContent:self.bestAttemptContent];
    
    [self contentComplete];
}

- (void)contentComplete
{
    self.contentHandler(self.bestAttemptContent);
}

@end
