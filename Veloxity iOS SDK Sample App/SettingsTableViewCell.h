//
//  SettingsTableViewCell.h
//  Veloxity iOS Sdk Sample App
//
//  Copyright © 2017 Veloxity. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UISwitch *switch_service;


@end
