//
//  SettingsTableViewController.h
//  Veloxity iOS Sdk Sample App
//
//  Copyright © 2017 Veloxity. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsTableViewController : UITableViewController

@end
