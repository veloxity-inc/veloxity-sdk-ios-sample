//
//  SettingsTableViewCell.m
//  Veloxity iOS Sdk Sample App
//
//  Copyright © 2017 Veloxity. All rights reserved.
//

#import "SettingsTableViewCell.h"
#import "AppDelegate.h"
#import <VeloxitySDK/VeloxitySDK.h>

@interface SettingsTableViewCell () <VeloxityDelegate>

@end

@implementation SettingsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self serviceStateControl];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)changeUserDataUsageStatus:(BOOL)status
{
    [self.switch_service setOn:[[Veloxity sharedInstance] serviceStatus]];
}

- (IBAction)switchChanged:(id)sender {
    if (self.switch_service.isOn) {
        [[Veloxity sharedInstance] setLicenseKey:@"<LICENSE_KEY>"];
        [[Veloxity sharedInstance] setWebServiceUrl:@"<WEB_SERVICE_URL"];
        [[Veloxity sharedInstance] setAuthorizationMenu:NSLocalizedString(@"AUTH_TITLE", nil) withMessage:NSLocalizedString(@"AUTH_MSG", nil) andAcceptTitle:NSLocalizedString(@"AUTH_OK", nil) andDenyTitle:NSLocalizedString(@"AUTH_CANCEL", nil)];
        [[Veloxity sharedInstance] setDelegate:self];
        [[Veloxity sharedInstance] optIn];
    } else {
        [[Veloxity sharedInstance] optOut];
    }
}

#pragma mark Veloxity Delegate Methods

- (void)vlxAuthorizationDidSucceed
{
    NSLog(@"VeloxitySDKApp - Authorization Did Succeed");
    [self changeUserDataUsageStatus:YES];
}

- (void)vlxAuthorizationDidFailed
{
    NSLog(@"VeloxitySDKApp - Authorization Did Failed");
    [self changeUserDataUsageStatus:NO];
}

- (void)serviceStateControl
{
    if ([[Veloxity sharedInstance] serviceStatus] == 0) {
        [self.switch_service setOn:NO];
    } else {
        [self.switch_service setOn:YES];
    }
}

@end
