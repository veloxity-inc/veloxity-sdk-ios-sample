
#import "AppDelegate.h"
#import <VeloxitySDK/Veloxity.h>

@interface AppDelegate () <VeloxityDelegate, UIAlertViewDelegate>

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication*)application didFinishLaunchingWithOptions:(NSDictionary*)launchOptions
{
    [[Veloxity sharedInstance] setLicenseKey:@"<LICENSE_KEY>"];
    [[Veloxity sharedInstance] setWebServiceUrl:@"<WEB_SERVICE_URL"];
    [[Veloxity sharedInstance] setAuthorizationMenu:NSLocalizedString(@"AUTH_TITLE", nil) withMessage:NSLocalizedString(@"AUTH_MSG", nil) andAcceptTitle:NSLocalizedString(@"AUTH_OK", nil) andDenyTitle:NSLocalizedString(@"AUTH_CANCEL", nil)];
    [[Veloxity sharedInstance] setDelegate:self];
    [[Veloxity sharedInstance] start];

    [self registerToAPN];
    return YES;
}

- (void)registerToAPN
{
    //Register For Push Notifications
    if([[[UIDevice currentDevice] systemVersion] floatValue] >= 10.0) {
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        center.delegate = self;
        [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error){
            if( !error ){
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[UIApplication sharedApplication] registerForRemoteNotifications];
                });
            }
        }];
    }
    else if([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
}

- (void)application:(UIApplication*)application didRegisterUserNotificationSettings:(UIUserNotificationSettings*)notificationSettings
{
    [application registerForRemoteNotifications];
}

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    [[Veloxity sharedInstance] registerDeviceToken:deviceToken];
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
    NSLog(@"error occured while registering the remote notifications: %@", error.description);
}

- (void)application:(UIApplication*)application didReceiveRemoteNotification:(NSDictionary*)userInfo
{
    if ([userInfo objectForKey:@"vlx"]) {
        [[Veloxity sharedInstance] startBackgroundTransactionWithUserInfo:userInfo];
        return;
    }

    NSLog(@"Not a VeloxitySDK PN message.");
}

- (void)application:(UIApplication*)application didReceiveRemoteNotification:(NSDictionary*)userInfo fetchCompletionHandler:(nonnull void (^)(UIBackgroundFetchResult))completionHandler
{
    if ([userInfo objectForKey:@"vlx"]) {
        [[Veloxity sharedInstance] startBackgroundTransactionWithUserInfo:userInfo];
        completionHandler(UIBackgroundFetchResultNewData);
        return;
    }

    NSLog(@"Not a VeloxitySDK PN message.");
}

- (void)vlxAuthorizationDidSucceed
{
    NSLog(@"VeloxitySDKApp - Authorization Did Succeed");
}

- (void)vlxAuthorizationDidFailed
{
    NSLog(@"VeloxitySDKApp - Authorization Did Failed");
}

#pragma mark - UNUserNotificationCenter Delegate // >= iOS 10

- (void)userNotificationCenter:(UNUserNotificationCenter*)center willPresentNotification:(UNNotification*)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler
{
    NSDictionary* userInfo = notification.request.content.userInfo;
    NSLog(@"willPresentNotification = %@", userInfo);
    if ([userInfo objectForKey:@"vlx"]) {
        [[Veloxity sharedInstance] startBackgroundTransactionWithUserInfo:userInfo];
    }
    completionHandler(UNNotificationPresentationOptionNone);
}

- (void)userNotificationCenter:(UNUserNotificationCenter*)center didReceiveNotificationResponse:(UNNotificationResponse*)response withCompletionHandler:(void(^)(void))completionHandler
{
    if ([response.actionIdentifier isEqualToString:UNNotificationDismissActionIdentifier]) {
        NSLog(@"UNNotificationDismissActionIdentifier");
    } else if ([response.actionIdentifier isEqualToString:UNNotificationDefaultActionIdentifier]) {
        NSLog(@"UNNotificationDefaultActionIdentifier");
    }
    
    NSDictionary* userInfo = response.notification.request.content.userInfo;
    NSLog(@"didReceiveNotificationResponse = %@", userInfo);
    if ([userInfo objectForKey:@"vlx"]) {
        [[Veloxity sharedInstance] startBackgroundTransactionWithUserInfo:userInfo];
        completionHandler();
        return;
    }
    completionHandler();
}

@end

